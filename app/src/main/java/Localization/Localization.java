package Localization;

/**
 * Created by Reiz3N on 2015-07-05.
 */
public class Localization {

    private double latitude, longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Localization(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }


}
