package Localization;

/**
 * Created by Reiz3N on 2015-07-05.
 */

        import android.app.Activity;
        import android.location.Address;
        import android.location.Location;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.Button;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.buczel.wudzitsu.R;
        import com.google.android.gms.common.ConnectionResult;
        import com.google.android.gms.common.GooglePlayServicesUtil;
        import com.google.android.gms.common.api.GoogleApiClient;
        import com.google.android.gms.location.LocationRequest;
        import com.google.android.gms.location.LocationServices;

public class Tracking implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // LogCat tag
    private static final String TAG = "Tracking";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;


    public double getLatitude() {return latitude;}

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private double  latitude, longitude;


    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    // UI elements
    private TextView lblLocation;
    private Button btnShowLocation, btnStartLocationUpdates;

    Activity wywolujaceActivity;
    char type;
    String userName, password;
    int carId;
    double amount, costPerLitre;

    public Tracking(Activity wywolujaceActivity){
        this.wywolujaceActivity = wywolujaceActivity;
    }


    public void Track(){

        checkPlayServices();

        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();


    }


    /**
     * Method to display the location on UI
     * */
    private synchronized void displayLocation() {

        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

            //lblLocation.setText(latitude + ", " + longitude);
//            Toast.makeText(wywolujaceActivity.getApplicationContext(),
//                    Double.toString(latitude), Toast.LENGTH_SHORT)
//                    .show();

            TextView lat = (TextView) ((Activity)wywolujaceActivity).findViewById(R.id.latitude);
            lat.setText(Double.toString(latitude));

            TextView lon = (TextView) ((Activity)wywolujaceActivity).findViewById(R.id.longitude);
            lon.setText(Double.toString(longitude));



        } else {
 //           lblLocation.setText("(Couldn't get the location. Make sure location is enabled on the device)");
//            Toast.makeText(wywolujaceActivity.getApplicationContext(),
//                    "(Eh)", Toast.LENGTH_SHORT)
//                    .show();
//
        }

    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(wywolujaceActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(wywolujaceActivity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, wywolujaceActivity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                                Toast.makeText(wywolujaceActivity.getApplicationContext(),
                                        "This device is not supported.", Toast.LENGTH_LONG)
                                        .show();

            }
            return false;
        }
        return true;
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

}

